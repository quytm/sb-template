package uet.hungnh.interview.sbtemplate.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import uet.hungnh.interview.sbtemplate.entity.ProductReview;

@Repository
public interface ReviewRepository extends CrudRepository<ProductReview, Long> {
}
