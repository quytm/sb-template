package uet.hungnh.interview.sbtemplate.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import uet.hungnh.interview.sbtemplate.entity.Product;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, String> {
    Page<Product> findAll(Pageable pageable);
}
