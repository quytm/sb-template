package uet.hungnh.interview.sbtemplate.service;

import uet.hungnh.interview.sbtemplate.dto.ReviewDTO;

public interface IReviewService {
    ReviewDTO submitReview(ReviewDTO reviewDTO);
}
