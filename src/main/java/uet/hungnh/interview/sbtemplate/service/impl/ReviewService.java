package uet.hungnh.interview.sbtemplate.service.impl;

import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uet.hungnh.interview.sbtemplate.dto.ReviewDTO;
import uet.hungnh.interview.sbtemplate.entity.ProductReview;
import uet.hungnh.interview.sbtemplate.repo.ReviewRepository;
import uet.hungnh.interview.sbtemplate.service.IReviewService;

@Service
@Transactional(rollbackFor = Exception.class)
public class ReviewService implements IReviewService {

    private final ReviewRepository reviewRepository;
    private final MapperFacade objectMapper;

    public ReviewService(ReviewRepository reviewRepository,
                         MapperFacade objectMapper) {
        this.reviewRepository = reviewRepository;
        this.objectMapper = objectMapper;
    }

    @Override
    public ReviewDTO submitReview(ReviewDTO reviewDTO) {
        ProductReview review = objectMapper.map(reviewDTO, ProductReview.class);
        review = reviewRepository.save(review);
        return objectMapper.map(review, ReviewDTO.class);
    }
}
