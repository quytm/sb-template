package uet.hungnh.interview.sbtemplate.service.impl;

import ma.glasnost.orika.MapperFacade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uet.hungnh.interview.sbtemplate.dto.ProductDTO;
import uet.hungnh.interview.sbtemplate.dto.ProductDTOs;
import uet.hungnh.interview.sbtemplate.entity.Product;
import uet.hungnh.interview.sbtemplate.repo.ProductRepository;
import uet.hungnh.interview.sbtemplate.service.IProductService;

import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class ProductService implements IProductService {

    private final ProductRepository productRepository;
    private final MapperFacade objectMapper;

    public ProductService(ProductRepository productRepository,
                          MapperFacade objectMapper) {
        this.productRepository = productRepository;
        this.objectMapper = objectMapper;
    }

    @Override
    public ProductDTOs getProductList(int pageIndex, int pageSize) {
        ProductDTOs productDTOs = new ProductDTOs();

        final PageRequest pageRequest = new PageRequest(pageIndex - 1, pageSize, new Sort(Sort.Direction.ASC, "productName"));
        Page<Product> productPage = productRepository.findAll(pageRequest);
        List<Product> products = productPage.getContent();

        List<ProductDTO> productDTOList = objectMapper.mapAsList(products, ProductDTO.class);
        productDTOs.setProductDTOList(productDTOList);

        Long totalItem = productRepository.count();
        productDTOs.setTotalItems(totalItem);

        return productDTOs;
    }
}
