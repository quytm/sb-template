package uet.hungnh.interview.sbtemplate.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uet.hungnh.interview.sbtemplate.dto.ReviewDTO;
import uet.hungnh.interview.sbtemplate.service.IReviewService;

@RestController
@RequestMapping("/api/reviews")
public class ReviewController {

    private final IReviewService reviewService;

    public ReviewController(IReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @PostMapping
    public ReviewDTO submitReview(@RequestBody ReviewDTO reviewDTO) {
        return reviewService.submitReview(reviewDTO);
    }
}
