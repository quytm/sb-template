package uet.hungnh.interview.sbtemplate.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uet.hungnh.interview.sbtemplate.dto.ProductDTOs;
import uet.hungnh.interview.sbtemplate.service.IProductService;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    private final IProductService productService;

    public ProductController(IProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public ProductDTOs getProductList(@RequestParam("pageIndex") int pageIndex,
                                      @RequestParam("pageSize") int pageSize) {
        return productService.getProductList(pageIndex, pageSize);
    }
}
